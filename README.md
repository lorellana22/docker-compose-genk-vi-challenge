# Docker compose

Dado un repositorio con tres proyectos, crear el Dockerfile para cada uno de los proyectos y orquestar los distintos servicios con un solo fichero de docker-compose.



Ten en cuenta que la aplicación devops-db-app necesita definir las siguientes variables de entorno:

    • DATABASE_PASSWORD

    • DATABASE_HOST

    • DATABASE_PORT

    • DATABASE_USERNAME

    • DATABASE_NAME

Y que la base de datos MySQL necesita definir estas otras variables de entorno:

    • MYSQL_ROOT_PASSWORD

    • MYSQL_DATABASE

    • MYSQL_USER

    • MYSQL_PASSWORD

**Importante**: el Dockerfile del proyecto devops-web tiene que ser multistage.

Para poder inspeccionar el contenido de APIs y base de datos, necesitamos tener instaladas las siguientes herramientas:

- Postman (https://www.postman.com/)
- Dbeaver (https://dbeaver.io/)

Como entregable pega la URL del repositorio que hayas creado a partir del original, donde ejecutando un docker-compose up -d, o docker compose up --build (para ver lo que sucede y siempre que hayamos hecho cambios de código), se debería levantar todo el ecosistema de aplicaciones.

---------------------------------

**En DBeaver**:

- Server Host: localhost
- Port: 3307
- Database: devops
- Username: devops
- Password: cogerla del fichero .env

Clickar en Test Connection y Finish.

Buscar la tabla "dog" en Databases

**En Postman**:
- GET: http://localhost:3000/dogs - send (nos devuelve un array vacío)
- Si añadimos un dato en la base de datos, nos devuelve el array con esos datos.


